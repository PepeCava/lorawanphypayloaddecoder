﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecodeBase64
{
    public class Converter
    {
        public string phyPayload { get; }
        public string MHDR {get; }
        public string MacPayload { get; }
        public string MIC { get; }
        private byte[] liby { get; }

        public Converter(string Phy)
        {
            phyPayload = Phy;
            liby = Convert.FromBase64String(Phy);
            MHDR = BitConverter.ToString(liby, 0, 1);
            MacPayload = BitConverter.ToString(liby, 1, liby.Length - 5);
            MIC = BitConverter.ToString(liby, liby.Length - 4, 4);
        }

        public string Mtype()
        {
            string Mtype;
            var temp = Convert.ToInt32(MHDR, 16);
            Mtype = Convert.ToString(temp, 2);
            while (Mtype.Length < 8)
            {
                Mtype = "0" + Mtype; 
            }
            return Mtype.Substring(0,3); 
        }
        public string RFU()
        {
            string Mtype;
            var temp = Convert.ToInt32(MHDR, 16);
            Mtype = Convert.ToString(temp, 2);
            while (Mtype.Length < 8)
            {
                Mtype = "0" + Mtype;
            }
            return Mtype.Substring(3, 3);
        }

        public string Major()
        {
            string Mtype;
            var temp = Convert.ToInt32(MHDR, 16);
            Mtype = Convert.ToString(temp, 2);
            while (Mtype.Length < 8)
            {
                Mtype = "0" + Mtype;
            }
            return Mtype.Substring(6, 2);
        }

        public string devAddr()
        {
            string bigI = BitConverter.ToString(liby.Skip(1).Take(4).ToArray());
            var teb = bigI.Split('-');

            string littleI ="";
            for (int i = 0; i < teb.Length; i++) littleI += teb[teb.Length - 1 - i] + "-";
 
            return littleI.Remove(littleI.Length - 1);
        }
        public string fctrl()
        {

            return BitConverter.ToString(liby.Skip(5).Take(1).ToArray());
        }

        public string fcnt()
        {

            return BitConverter.ToString(liby.Skip(6).Take(2).ToArray());
        }

        private int foptssize()
        {
            string fc = fcnt();
            var temp = Convert.ToInt32(fcnt().Replace("-", ""), 16);
            var b2fcnt = Convert.ToString(temp, 2);
            int size = 0; 
            for(int i = 0; i<3; i++)
            {
                if(b2fcnt[b2fcnt.Length-3+i] == '1')
                {
                    size += (int)Math.Pow(2, 2 - i);
                }
            }
            return size; 
        }

        public string fhdr()
        {
            int size = foptssize();
            return BitConverter.ToString(liby.Skip(1).Take(7+size).ToArray());

        }

        public string fopts()
        {
            int size = foptssize();
            if (size == 0) return ""; 
            return BitConverter.ToString(liby.Skip(8).Take(size).ToArray());
        }


        public string fport()
        {
            int size = foptssize();
            if(liby.Length-5-7-size == 0)
            {
                return "";
            }
            else
            {
                return BitConverter.ToString(liby.Skip(8+size).Take(1).ToArray());
            }

        }
        public string frm()
        {
            int size = foptssize();
            if (liby.Length - 5 - 7 - size == 0)
            {
                return "";
            }
            else
            {
                return BitConverter.ToString(liby.Skip(9 + size).Take(liby.Length -(9+size+4)).ToArray());
            }
        }
        /*public static bool GetBit(this byte b, int bitNumber)
        {
            return (b & (1 << bitNumber)) != 0;
        }*/
    }
}
