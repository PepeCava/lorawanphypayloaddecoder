﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DecodeBase64
{
    public class Decrypter
    {

        public static string DecryptFRM(string frmPayload, string keyString, byte[] fnc, byte[] devadr)
        {
            byte[] key = new byte[16];
            int count = 0;
            foreach (string s in keyString.Split(' '))
            {
                key[count] = BitConverter.GetBytes(Convert.ToInt32(s, 16))[0];
                count++;
            }    
            List<byte> frmtobyte = new List<byte>();
            foreach (var c in frmPayload.Split('-')) frmtobyte.Add(BitConverter.GetBytes(Convert.ToInt32(c, 16))[0]);

            int k = (int)Math.Ceiling(Convert.ToDouble(frmtobyte.Count) / 16.0);
            List<byte[]> S = new List<byte[]>();

            Aes aes = Aes.Create();
            aes.Mode = CipherMode.ECB;
            aes.Padding = PaddingMode.None;
            aes.Key = key;
            var encryptor = aes.CreateEncryptor();
            var decryptor = aes.CreateDecryptor();
            byte[][] array1 = new byte[(int)k][];
            byte[][] array2 = new byte[(int)k][];
            for (int i = 1; i <= k; i++)
            {
                array1[i - 1] = new byte[16];
                array1[i - 1][0] = 1;
                array1[i - 1][1] = 0;
                array1[i - 1][2] = 0;
                array1[i - 1][3] = 0;
                array1[i - 1][4] = 0;
                array1[i - 1][5] = 0;
                array1[i - 1][6] = devadr[0];
                array1[i - 1][7] = devadr[1];
                array1[i - 1][8] = devadr[2];
                array1[i - 1][9] = devadr[3];
                array1[i - 1][10] = fnc[1];
                array1[i - 1][11] = fnc[0];
                array1[i - 1][12] = 0;
                array1[i - 1][13] = 0;
                array1[i - 1][14] = 0;
                array1[i - 1][15] = (byte)i;
                array2[i - 1] = encryptor.TransformFinalBlock(array1[i - 1], 0, 16);
            }

            byte[] pldPad16 = new byte[16]; // pld | pad16

            for (int i = 0; i < pldPad16.Length; i++)
            {
                if (i < frmtobyte.Count)
                    pldPad16[i] = frmtobyte[i];
            }

            foreach (byte[] item in array2)
            {
                for (int i = 0; i < 16; i++)
                {
                    pldPad16[i] ^= item[i]; // (pld | pad16) xor S
                }
            }
            return Encoding.ASCII.GetString(pldPad16);
        }

        public static string decryptPhy(byte[] b, string keyString)
        {
            int fOptLen = b[5] & 15; // la longueur de fOpt est les 4 derniers bits
            byte[] frmPayload = new byte[b.Length - 1 - 4 - 1 - 2 - 1 - 4 - fOptLen]; // calculé grâce au diagramme

            int j = 0;
            for (int i = 9 + fOptLen; i < b.Length - 4; i++)
            {
                frmPayload[j] = b[i];
                j++;
            } // on extrait frmPayload du reste du message

            Aes aes = Aes.Create(); //System.Security.Cryptography
            aes.Mode = CipherMode.ECB;
            aes.Padding = PaddingMode.None;
            byte[] key = new byte[16];
            int count = 0;
            foreach (string s in keyString.Split(' '))
            {
                key[count] = BitConverter.GetBytes(Convert.ToInt32(s, 16))[0];
                count++;
            }
            aes.Key = key;
            var encryptor = aes.CreateEncryptor();

            double k = Math.Ceiling((double)frmPayload.Length / 16.0);
            byte[][] array1 = new byte[(int)k][]; // équivalent à A dans l'algorhytme
            byte[][] array2 = new byte[(int)k][]; // équivalent à S dans l'algorhytme
            for (int i = 1; i <= k; i++)
            {
                array1[i - 1] = new byte[16];
                array1[i - 1][0] = 1;
                array1[i - 1][1] = 0;
                array1[i - 1][2] = 0;
                array1[i - 1][3] = 0;
                array1[i - 1][4] = 0;
                array1[i - 1][5] = 0;
                array1[i - 1][6] = b[1];
                array1[i - 1][7] = b[2];
                array1[i - 1][8] = b[3];
                array1[i - 1][9] = b[4];
                array1[i - 1][10] = b[6];
                array1[i - 1][11] = b[7];
                array1[i - 1][12] = 0;
                array1[i - 1][13] = 0;
                array1[i - 1][14] = 0;
                array1[i - 1][15] = (byte)i;

                array2[i - 1] = encryptor.TransformFinalBlock(array1[i - 1], 0, 16);
            }
            byte[] pldPad16 = new byte[16]; // pld | pad16

            for (int i = 0; i < pldPad16.Length; i++)
            {
                if (i < frmPayload.Length)
                    pldPad16[i] = frmPayload[i];
            }

            foreach (byte[] item in array2)
            {
                for (int i = 0; i < 16; i++)
                {
                    pldPad16[i] ^= item[i]; // (pld | pad16) xor S
                }
            }
            return Encoding.ASCII.GetString(pldPad16).Substring(0, frmPayload.Length);
        }
    }
}
