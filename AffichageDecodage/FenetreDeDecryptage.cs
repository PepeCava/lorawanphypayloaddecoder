﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;

namespace AffichageDecodage
{
    public partial class FenetreDeDecryptage : Form
    {
        string frmPayload;
        string phypayload;
        byte[] adr;
        byte[] fncount;
        public FenetreDeDecryptage(string phy, string frm, string adr, string fnc)
        {
            InitializeComponent();
            phypayload = phy; 
            textBox1.Text = "dd 0a 32 bf 8b 40 82 bc 4a 00 17 e9 9c 15 17 d6";
            frmPayload = frm;
            this.Name = frm;            
            this.adr = BitConverter.GetBytes(Convert.ToInt32(adr.Replace("-", ""), 16));            
            this.fncount = BitConverter.GetBytes(Convert.ToInt32(fnc.Replace("-", ""), 16));
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 0) return;
            label2.Visible = true;
            textBox2.Text = DecodeBase64.Decrypter.decryptPhy(Convert.FromBase64String(phypayload), textBox1.Text);           
            textBox2.Visible = true;
        }
    }
}
