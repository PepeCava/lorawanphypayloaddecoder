﻿using DecodeBase64;
using MQTTnet;
using MQTTnet.Adapter;
using MQTTnet.Client;
using MQTTnet.Client.Options;
using MQTTnet.Diagnostics;
using MQTTnet.Exceptions;
using MQTTnet.Implementations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace AffichageDecodage
{
    public partial class FenetreDeConnexion : Form
    {
        IMqttClient client;
        Thread thread;
        Dictionary<string, string> dictionnaryDeviceKey = new Dictionary<string, string>();
        List<string> dicFile = new List<string>();
        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }
        string fileName = AssemblyDirectory + "\\dicFile.txt";
        public FenetreDeConnexion()
        {
            InitializeComponent();
            textBox1.Text = "loraserver.tetaneutral.net";
            textBox2.Text = "1883";
            if (File.Exists(fileName))
            {
                foreach(var s in  System.IO.File.ReadAllLines(fileName)) dicFile.Add(s);
                foreach(var c in dicFile.ToArray())
                {
                    var t = c.Split(':');
                    dictionnaryDeviceKey.Add(t[0], t[1]);
                }
            }
            else
            {
                //System.IO.File.Create(fileName);
                addValueToDic("018E4B26", "DD 0A 32 BF 8B 40 82 BC 4A 00 17 E9 9C 15 17 D6");
            }
        }

        private async void button2_Click(object sender, EventArgs e)
        {
            // servira à boucler jusqu'à la récéption d'un message

            var f = new MqttFactory();
            client = f.CreateMqttClient();

            var options = new MqttClientOptionsBuilder().WithTcpServer(textBox1.Text, Convert.ToInt32(textBox2.Text)).Build();
            try
            {
                await client.ConnectAsync(options, CancellationToken.None);
            }
            catch (MqttCommunicationTimedOutException mqtte) { MessageBox.Show("Timeout Exception : \n" + mqtte.ToString(), "Timeout", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }
            groupBox1.Visible = true;
            thread = new Thread(new ThreadStart(getMessage));
            thread.Start();
        }

        private async void getMessage()
        {
            while (true)
            {
                string phyPayload = "";
                bool toReceive = true;
                client.UseApplicationMessageReceivedHandler(E =>
                {
                    string payLoad = Encoding.UTF8.GetString(E.ApplicationMessage.Payload);
                    char splitter = '\"';
                    phyPayload = payLoad.Split(splitter)[3];
                    toReceive = false;
                }); // Cette fonction sera appelée quand un message est reçu
                await client.SubscribeAsync(new TopicFilterBuilder().WithTopic("gateway/3150000000000002/event/up").Build());
                while (toReceive) ;
                Converter phy = new Converter(phyPayload);
                try
                {
                    dataGridView1.Invoke((MethodInvoker)(() => dataGridView1.Rows.Add(phy.phyPayload, Decrypter.decryptPhy(Convert.FromBase64String(phy.phyPayload), dictionnaryDeviceKey[phy.devAddr().Replace(" ", "").Replace("-", "").ToUpper()]))));
                }
                catch { }

                toReceive = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count < 1) return;
            if (thread.ThreadState == System.Threading.ThreadState.Running) thread.Abort();
            string lines = "";
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++) lines += "{\n \"phypayload\" : \"" + dataGridView1.Rows[i].Cells[0].Value + "\",\n \"frmpayload décryptée\" : \"" + dataGridView1.Rows[i].Cells[1].Value + "\"\n}";
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                string path = folderBrowserDialog1.SelectedPath;
                System.IO.File.WriteAllText(path + "/log.txt", lines);
            }
            thread = new Thread(new ThreadStart(getMessage));
            thread.Start();
        }

        public void addValueToDic(string d, string k)
        {
            dictionnaryDeviceKey.Add(d, k);
            dicFile.Add(d + ":" + k);
            File.WriteAllLines(fileName, dicFile.ToArray());
        }

        private void ajouterUneCléToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new KeyAdder(this).ShowDialog(); 
        }

        private void modifierLesClésToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(fileName);
        }
    }
}
