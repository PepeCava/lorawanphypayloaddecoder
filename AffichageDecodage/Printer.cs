﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AffichageDecodage
{
    public partial class Printer : Form
    {
        DecodeBase64.Converter conv; 
        public Printer(string phy)
        {
            conv = new DecodeBase64.Converter(phy); 
            InitializeComponent();
            this.Text = "phyPayload : " + conv.phyPayload; 
            textBox1.Text = conv.MHDR;
            textBox2.Text = conv.MacPayload;
            textBox3.Text = conv.MIC;
            textBox6.Text = conv.Mtype();
            textBox5.Text = conv.RFU();
            textBox4.Text = conv.Major(); 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (groupBox1.Visible)
            {
                button1.Text = "Détails de MACPayload";
                groupBox1.Visible = false; 
            }
            else
            {
                generateMac();
                groupBox1.Visible = true;
                button1.Text = "Fermer les détails";
            }
            

        }

        private void generateMac()
        {
            textBox9.Text = conv.fhdr();
            textBox13.Text = conv.devAddr();
            textBox10.Text = conv.fctrl();
            textBox11.Text = conv.fcnt();
            textBox12.Text = conv.fopts();
            textBox7.Text = conv.frm();
            textBox8.Text = conv.fport();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox7.Text.Length == 0) return;
            FenetreDeDecryptage fed = new FenetreDeDecryptage(conv.phyPayload, textBox7.Text, textBox13.Text, textBox11.Text);
            fed.ShowDialog(); 
        }
    }
}
