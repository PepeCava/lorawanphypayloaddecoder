﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AffichageDecodage
{
    public partial class KeyAdder : Form
    {
        FenetreDeConnexion fdc; 
        public KeyAdder(FenetreDeConnexion fc)
        {
            InitializeComponent();
            fdc = fc; 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string device = textBox1.Text.Replace(" ", "").Replace("-", "").ToUpper();
            string key = textBox2.Text;
            fdc.addValueToDic(device, key);
            this.Dispose();
        }
    }
}
